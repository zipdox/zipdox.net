const hiddenVid = document.getElementById('hiddenvid');
const dummyVid = document.getElementById('dummyvid');
const vid = document.getElementById('vid');
const dl = document.getElementById('dl');

document.getElementById('gen').onclick = async function(){
    const hiddenFile = hiddenVid.files[0];
    if(!hiddenFile) return;
    const dummyFile = dummyVid.files[0];
    if(!dummyFile) return;

    const hiddenFileBuf = await hiddenFile.arrayBuffer();
    const hiddenFileArr = new Uint8Array(hiddenFileBuf);
    const dummyFileBuf = await dummyFile.arrayBuffer();
    const dummyFileArr = new Uint8Array(dummyFileBuf);

    // parse the hidden video
    const webmEBML = new EBML(hiddenFileArr);
    const segmentsElement = webmEBML.elements.find(element => element.id == 139690087);
    const segmentsEBML = new EBML(segmentsElement.data);

    // parse the dummy video
    const dummyEBML = new EBML(dummyFileArr);
    const dummySegmentsElement = dummyEBML.elements.find(element => element.id == 139690087);
    const dummySegmentsEBML = new EBML(dummySegmentsElement.data);
    
    const tracksElement = segmentsEBML.elements.find(element => element.id == 106212971);
    const tracksEBML = new EBML(tracksElement.data);

    // corrupt the hidden tracks by adding a track type element with type 3
    const numOriginalTracks = tracksEBML.elements.length;
    for(let i = 0; i < numOriginalTracks; i++){
        let trackData = tracksEBML.elements[i].data;
        let track = new EBML(trackData);
        track.elements = track.elements.filter(element => element.id != 8); // remove FlagDefault
        track.elements.push(new EBMLChunk(3, Uint8Array.from([3]))); // track type 3 = "complex"
        tracksEBML.elements[i].data = track.export();
    }

    // add the tracks from the dummy video but increase the track number to not override the hidden video's track numbers
    let dummyTracks = dummySegmentsEBML.elements.find(element => element.id == 106212971);
    let dummyTracksEBML = new EBML(dummyTracks.data);

    for(let i = 0; i < dummyTracksEBML.elements.length; i++){
        let trackData = dummyTracksEBML.elements[i].data;
        let track = new EBML(trackData);
        track.elements = track.elements.filter(element => element.id != 8); // remove FlagDefault
        track.elements.find(element => element.id == 87).data[0] += numOriginalTracks;
        dummyTracksEBML.elements[i].data = track.export();
        tracksEBML.elements.push(dummyTracksEBML.elements[i]);
    }

    tracksElement.data = tracksEBML.export();

    // get clusters from dummy video and add them to the hidden video
    const dummyClusters = dummySegmentsEBML.elements.filter(element => element.id == 256095861);
    for(let dummyCluster of dummyClusters){
        let dummyClusterEBML = new EBML(dummyCluster.data);
        for(let i = 0; i < dummyClusterEBML.elements.length; i++){
            if(dummyClusterEBML.elements[i].id == 103) continue;
            dummyClusterEBML.elements[i].data[0] += numOriginalTracks;
        }
        dummyCluster.data = dummyClusterEBML.export();
    }

    let insertIdx = 1;
    for(; insertIdx < segmentsEBML.elements.length; insertIdx++){
        if(segmentsEBML.elements[insertIdx].id == 256095861) break;
    }
    segmentsEBML.elements.splice(insertIdx, 0, ...dummyClusters);

    segmentsElement.data = segmentsEBML.export();

    const finalFile = webmEBML.export();

    const finalBlob = new Blob([finalFile], {type: 'video/webm'});

    dl.href = vid.src = URL.createObjectURL(finalBlob);
    dl.textContent = dl.download = hiddenFile.name;
    
}
