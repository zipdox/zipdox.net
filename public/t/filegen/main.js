document.getElementById('generate').onclick = function(){
    const filesize = document.getElementById('filesize').value;
    const file = new Uint8Array(filesize);
    if(document.getElementById('randomize').checked){
        const start = performance.now();
        for(let i = 0; i < filesize; i++){
            file[i] = Math.round(Math.random() * 255);
        }
    }
    const downloadBlob = new Blob([file], {type: 'application/octet-stream'});
    const downloadURL = URL.createObjectURL(downloadBlob);
    console.log(downloadURL);

    const downloadButton = document.createElement('a');
    downloadButton.download = 'file.bin';
    downloadButton.href = downloadURL;
    document.body.appendChild(downloadButton);
    downloadButton.click();
    downloadButton.remove();
}

function toggleRandom(){
    const checkBox = document.getElementById('randomize');
    checkBox.checked = !checkBox.checked;
}