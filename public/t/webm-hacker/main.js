const file = document.getElementById('file');
const duration = document.getElementById('duration');
const write = document.getElementById('write');

function parseArbitraryInt(array){
    let number = 0;
    for(let byte = 0; byte < array.length; byte++){
        number |= (array[byte] << ((array.length - 1 - byte) * 8)); 
    }
    return number;
}

function parseFloat64(array){
    const buffer = array.buffer.slice(array.byteOffset, array.byteLength + array.byteOffset);
    const view = new DataView(buffer);
    if(view.byteLength == 4){
        return view.getFloat32(0);
    }else if(view.byteLength == 8){
        return view.getFloat64(0);
    }
}

function createFloat(number){
    const buffer = new ArrayBuffer(8);
    const view = new DataView(buffer);
    view.setFloat64(0, number);
    return new Uint8Array(buffer);
}

file.onchange = async function(){
    if(file.files[0] == undefined) return;

    const fileName = file.files[0].name;
    const fileData = await file.files[0].arrayBuffer();
    const fileArr = new Uint8Array(fileData);

    const webmEBML = new EBML(fileArr);
    const segmentsElement = webmEBML.elements.find(element => element.id == 139690087);
    const segmentsEBML = new EBML(segmentsElement.data);
    const infoElement = segmentsEBML.elements.find(element => element.id == 88713574);
    const infoEBML = new EBML(infoElement.data);

    const timestampScaleElement = infoEBML.elements.find(element => element.id == 710577);
    const timestampScale = parseArbitraryInt(timestampScaleElement.data);

    const durationElement = infoEBML.elements.find(element => element.id == 1161);
    const durationFloat = parseFloat64(durationElement.data);
    duration.value = durationFloat * timestampScale / 1000000000;
    window.selectedFile = {fileName, webmEBML, segmentsElement, segmentsEBML, infoElement, infoEBML, timestampScale, durationElement};
}

write.onclick = function(){
    const {fileName, webmEBML, segmentsElement, segmentsEBML, infoElement, infoEBML, timestampScale, durationElement} = window.selectedFile;
    const durationFloat = duration.value * 1000000000 / timestampScale;
    const durationFloatData = createFloat(durationFloat);

    durationElement.data = durationFloatData;
    infoElement.data = infoEBML.export();
    segmentsEBML.elements = segmentsEBML.elements.filter(element=>element.id != 39109479);
    segmentsElement.data = segmentsEBML.export();
    const fileData = webmEBML.export();
    
    
    const downloadBlob = new Blob([fileData], {type: 'video/webm'});
    const downloadURL = URL.createObjectURL(downloadBlob);

    const downloadButton = document.createElement('a');
    downloadButton.download = fileName;
    downloadButton.href = downloadURL;
    document.body.appendChild(downloadButton);
    downloadButton.click();
    downloadButton.remove();
}
