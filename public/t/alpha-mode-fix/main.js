const file = document.getElementById('file');

// const alphaTag = new EBMLChunk(13171, Uint8Array.from([0x63, 0xc0, 0x80, 0x67, 0xc8, 0x91, 0x45, 0xa3, 0x8a, 0x41, 0x4c, 0x50, 0x48, 0x41, 0x5f, 0x4d, 0x4f, 0x44, 0x45, 0x44, 0x87, 0x81, 0x31]));

file.onchange = async function(){
    if(file.files[0] == undefined) return;

    const fileName = file.files[0].name;
    const fileData = await file.files[0].arrayBuffer();
    const fileArr = new Uint8Array(fileData);

    const webmEBML = new EBML(fileArr);
    const segmentsElement = webmEBML.elements.find(element => element.id == 139690087);
    const segmentsEBML = new EBML(segmentsElement.data);

    const tracksElement = segmentsEBML.elements.find(element => element.id == 106212971);
    const tracksEBML = new EBML(tracksElement.data);

    for(let trackElement of tracksEBML.elements){
        let trackEBML = new EBML(trackElement.data);
        let trackType = trackEBML.elements.find(element => element.id == 3);
        if(trackType.data[0] != 1) continue;
        let trackVideo = trackEBML.elements.find(element => element.id == 96);
        let trackVideoEBML = new EBML(trackVideo.data);
        trackVideoEBML.elements.push(new EBMLChunk(5056, Uint8Array.from([1])));
        trackVideo.data = trackVideoEBML.export();
        trackElement.data = trackEBML.export();
    }

    tracksElement.data = tracksEBML.export();

    // const tagsElement = segmentsEBML.elements.find(element => element.id == 39109479);
    // const tagsEBML = new EBML(tagsElement.data);
    // tagsEBML.elements.unshift(alphaTag);
    // tagsElement.data = tagsEBML.export();

    segmentsElement.data = segmentsEBML.export();
    const downloadData = webmEBML.export();

    const downloadBlob = new Blob([downloadData], {type: 'video/webm'});
    const downloadURL = URL.createObjectURL(downloadBlob);

    const downloadButton = document.createElement('a');
    downloadButton.download = fileName;
    downloadButton.href = downloadURL;
    document.body.appendChild(downloadButton);
    downloadButton.click();
    downloadButton.remove();
}
