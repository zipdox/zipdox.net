class VINTStream{
    /**
     * Callback for when the VINT is finished
     * @callback vintCallback
     * @param {number} value - Value of the VINT
     * @param {number} width - The width of the VINT
     * @param {Uint8Array} leftover - The leftover data that wasn't part of the VINT
     */

    /**
     * Create VINT stream parser
     * @param {vintCallback} resolve - Callback for when the VINT is finished
     */
    constructor(resolve){
        this.resolve = resolve;
        this.state = 0;
        this.width = 0;
        this.byteIdx = 0;
    }

    write(data){
        // loop through each byte
        for(let i = 0; i < data.length; i++){
            let byte = data[i];

            // loop through each bit
            for(let b = 7; b >= 0; b--){
                let bit = byte >> b & 1;

                // if the bits string is undefined that means that we're still counting the VINT_WIDTH
                if(this.bits == undefined){
                    if(bit == 0){
                        this.width++;
                    }else{
                        this.width++;
                        // if we encounter a 1 that means that we reached end VINT_MARKER bit so we set the string to start collecting VINT_DATA bits
                        this.bits = '';
                    }
                }else{
                    this.bits += String(bit);
                }
            }

            this.byteIdx++;

            // the VINT_WIDTH is larger than 8 so we need to read more bytes
            if(this.width == 0) continue;

            // if we processed the last byte of the VINT we can resolve
            if(this.byteIdx >= this.width){
                const value = parseInt(this.bits, 2);
                const leftover = data.subarray(i+1);
                this.resolve(value, this.width, leftover);
                return;
            }
        }
    }
}

function concatUint8Arrays(arrays){
    let size = 0;
    for(let arr of arrays) size += arr.byteLength;

    const result = new Uint8Array(size);

    let offset = 0;
    for (let arr of arrays) {
        result.set(arr, offset);
        offset += arr.byteLength;
    }

    return result;
}

/** Stream EBML data and get callbacks for elements */
class EBMLStream{
    /**
     * Callback to fire when an ID was parsed
     * @callback idCallback
     * @param {number} id - ID of the parsing element
     * @returns {boolean} stop - Return true to terminate parsing
     */

    /**
     * Callback to fire when data size was parsed
     * @callback sizeCallback
     * @param {number} size - Size of the parsing element
     * @returns {boolean} stop - Return true to terminate parsing
     */

    /**
     * Callback to fire when data was parsed
     * @callback dataCallback
     * @param {Uint8Array} data - Data of the parsing elemenet
     * @returns {boolean} stop - Return true to terminate parsing
     */

    /**
     * Create an EBML stream parser
     * @param {idCallback} idCallback - Callback to fire when an ID was parsed
     * @param {sizeCallback} sizeCallback - Callback to fire when data size was parsed
     * @param {dataCallback} dataCallback - Callback to fire when data was parsed
     */
    constructor(idCallback, sizeCallback, dataCallback){
        this.idCallback = idCallback;
        this.sizeCallback = sizeCallback;
        this.dataCallback = dataCallback;
        this.state = 0;
    }

    /**
     * Write data to the stream
     * @param {Uint8Array} data - Data to write to the stream
     */
    write(data){
        const that = this;
        if(this.state == 0){
            if(this.vintID == undefined) this.vintID = new VINTStream(function(result, width, leftover){
                if(that.idCallback) that.idCallback(result);
                that.state++;
                delete that.vintID;
                if(leftover.length > 0) that.write(leftover);
            });
            this.vintID.write(data);
        }else if(this.state == 1){
            if(this.vintSize == undefined) this.vintSize = new VINTStream(function(result, width, leftover){
                that.dataSize = result;
                if(that.sizeCallback) that.sizeCallback(result);
                that.state++;
                delete that.vintSize;
                that.dataIdx = 0;
                if(leftover.length > 0) that.write(leftover);
            });
            this.vintSize.write(data);
        }else{
            const dataLeft = this.dataSize - this.dataIdx;
            const dataPart = data.subarray(0, dataLeft);
            const leftover = data.subarray(dataLeft)

            if(this.dataCallback) this.dataCallback(dataPart);

            this.dataIdx += dataPart.length;
            if(this.dataIdx >= this.dataSize){
                this.state = 0;
            }

            if(leftover.length > 0) this.write(leftover);
        }
    }
}

function createVINT(num){
    if(num == 0) return Uint8Array.from([128]);
    let bits = 0;
    let n = num;
    while(n > 0){
        bits++;
        n >>>= 1;
    }
    let octets = Math.ceil(bits / 7);

    let vintArr = new Uint8Array(octets);
    vintArr[0] = 1 << (8 - octets);
    for(let i = 0; i < octets; i++){
        vintArr[i] |= num >> (8 * (octets - i - 1));
    }
    return vintArr;
}

class EBMLChunk{
    /**
     * Create an EBML chunk
     * @param {number} id - EBML ID
     * @param {Uint8Array} data - EBML data
     */
    constructor(id, data){
        this.id = id;
        this.data = data;
    }

    /**
     * Exports the EBML chunk
     * @returns {Uint8Array} The exported EBML chunk
     */
    export(){
        const idVint = createVINT(this.id);
        const sizeVint = createVINT(this.data.length);
        return concatUint8Arrays([idVint, sizeVint, this.data]);
    }
}

class EBML{
    /**
     * Parse EBML
     * @param {Uint8Array} data - EBML Data to parse
     */
    constructor(data){
        this.elements = [];
        let leftover = data;
        while(leftover.length > 0){
            let id, size, data;
            new VINTStream(function(val, w, left){
                id = val;
                leftover = left;
            }).write(leftover);
            new VINTStream(function(val, w, left){
                size = val;
                leftover = left;
            }).write(leftover);
            data = leftover.subarray(0, size);
            leftover = leftover.subarray(size);
            this.elements.push(new EBMLChunk(id, data));
        }
    }

    static create(elements){
        const ebml = new EBML(new Uint8Array());
        ebml.elements = elements;
        return ebml;
    }

    /**
     * Exports the EBML data
     * @returns {Uint8Array} The exported EBML data
     */
    export(){
        return concatUint8Arrays(this.elements.map(elem => elem.export()));
    }
}
