-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: 21C0 ABD9 9CE6 36D2 8E04  9C8A 526A 6247 AF55 9A83
Comment: Zipdox <zipdox@zipdox.net>

xsFNBGXrexkBEADCc70bIijvQzNVJN0C6+/fgvPofs8h5qZp5PUC+r4u7oLDZV8U
9wcri4iAxaYmlX05efGdgNmaOJiqHvxaSu700/RRl20dDihf574ZZNjf/Jqo4Gpf
nvZRhr0k/MY2x94n2INTrUnt8RedDLNWjowHtqHyjX9+Se9PJR5fuOXqNhtgjasD
4OLpkNKeBva3gGQTTn2vyXhbKjrpwDhsrtREy/gqByPqriCv3CcVNxLYDRCF8SLU
aurZv34sy8gUbwgon01+r3yUni3UCHy+x0QmGy3hHkrqBw6XcEmnsSxz/0MmLlOp
q9LTtittATI92RyYItt9FP/noXVtn1JuW65fS5UwrZ++YZCz7AB3XriYiRUATGAd
IggrOKpqnQri4KASZ/ZxUJajBeiZ9g94hKimEwX+IOZPaxtZcDnyzQ4D5rhfC5XP
Q1YE/TY4JK+HRqP/SXET9LEgNwScUEuuaL6NCKM6T6lp8TqU4DMYu1/faw9+zWl4
p5/11XQ6BaxccrkmEDlJ0KIuITipVPq6XCafn9ExM8wLxgpcD5SKRAT7r1yWEtG8
MjOcSfnYSsODjjlidu73bQfurjducV92+MUd0VIsxsV7QY1EEtlxH23epuH0iatF
5Z/jjH7E5/urfa/p4KzcaWFq8sL/q4ELlMvkmHGUxqzOU+kK8sxafmSX4wARAQAB
zRpaaXBkb3ggPHppcGRveEB6aXBkb3gubmV0PsLBjgQTAQoAOBYhBCHAq9mc5jbS
jgScilJqYkevVZqDBQJl63sZAhsDBQsJCAcCBhUKCQgLAgQWAgMBAh4BAheAAAoJ
EFJqYkevVZqDr0cP/iADW1Lxp/f8Rhy9Idg8yk0kmnsMIvEsGhUWbdkRbq+yqBad
Q5Ml5wy0ELJx0rHbaFgqQqvTfXVr6efiVXH6oBUfOtCxErzx87YrbeVOpTNbL7sn
pNTRKLnxqcsUbABKvEGRIYVtVGCz3Qi6l0+sKzM5fQSfCjf5Uw+uywd6Sqop59Fj
ae34M8s479Z0IJnRwVac5XhzN/L0zx3O3BNsH6gJ8EEtK/ETiHuLQGLjJ5lEsIxR
dX1QuEdXVJrlpZ3AdOuebFR0Wy0/vkihkj2NIoEmpARc/g7Vyt0CVzwt/WOshi99
b2n62l0Xg1iAva+LZJu0sBQaZy0KAjK6K8zaGquOFsN65FfpZjpmBB5f2WFDQurD
L9BHAEt+as1vl0ml2Y2esOoioy5SKmwWEAslMjgh0XCzGpDrml7MnvAZXqPDFkM2
QdbhD8oKufz4f3B1EFNEM2h6laLmKMasbi8uu2684iQjF7oIDDfPeA3SdTE6T1AD
Reoy/yD9gX8UKsiQh+rm8/y6u9XzMmZUpS4PczVVmjm7boDeLbDkMl+njN8docty
hPECF5PsEMdrQe5lKOWXVcb8KWiP2A2TLlWVal6uo+tiAaW1DEMjQO2bIX4PVsHm
wMhW+Z03tkIh1MoRF2l9Zg5VLZKEMxSCrjmH7qMpNiauPHXNbOxOSdcdoB+rzsFN
BGXrexkBEADL9FWuWnuh5NJ0yUuUfgc07UZBvLivNCugndvUFhM7Q2VzRIg5hlBr
IrD6sxwaBPdf7zZddEzSsmSMRt20Z02Cz/ZOnI2XRRAB/2iexb2SI9vRckgE3K/W
D9TmokhFnVoQdGZcnncUeneWe/KzoxZb84X8QXtfenRz57VUi6VOH9tQiylu+EMX
OafQqaM+S7EieINQkYpqoSEzKviJ/6D2gJcWQhhMc2ZrkSZ0tv/vs0VUpmiyFl78
tiiimOoQ1KK2gVCUyDtBt2JOKv8SuzFrgXggDHPfFUDi8NKovBYn4kq2dDsAQOjW
+CGrjeF5M4GakRbey37CdkcFziXxdHEbE6uFnKMAXcZFQ/5Gezl5IRDAU001xU9q
KDTUunRn+xIhSSYrAISbi4yaI74MwGmtqX/zu+4lnbFGnhWgwJknWHfxxK/YsRtK
bEyc1kO2Kn/CYu6WVluEqvCuPgzPCPT/tJFxcWuMPwZjspFHMV6u4+nEwI5xk0HM
2kpDkO5J5+rHd6FRRWPQgp54XX66B7p/OkGDhMRU74oJsgS8B498XH4YC562huHG
OEkg+ewGgGGEzt8IaYd7AlOcgsVsHXob9MA2KPN2HBaAvqcNwgoF01f0iCoGzVO2
a8IYwupfHois1jv4aF+mrnInih7m4q1Oz6yAMLKxim0J7VSrTmIwUwARAQABwsF2
BBgBCgAgFiEEIcCr2ZzmNtKOBJyKUmpiR69VmoMFAmXrexkCGwwACgkQUmpiR69V
moPrfw/+N8X3YS9PW9283nsUozAot7ARRY4aRAkYYC9x/fs8/zXM+kBnb4nanjtX
wRJYge14q3+t7FmOLDnULrCUn4xJrK7dwz19KQiWYjTPmPHrHKspuW4U5lhZMvKD
t1aDVQGH+j/PVWNBHkSHmjCXsKgJF/QH5tlf2Ygkwl7jEECwjN3iRrESVnZScBkh
b5mr8qXagItWWr7vvisRrKxVqrQkUPjRCnXerRLnN3hPkvYlXs5Kcl8JtshajWTf
lBTO/gyfJHjOA2F8OyZFYpxcfv2S1sCt5RQE3Zut2/Cu2zl6JtyiEuOZqCrDz8D2
I63MARPx2npxZO2nQeuIAZbw1bW/IH8r04fIetaCmS/uaJ4u+qHpXS57rRKOqYrK
wuUdTkhdfdM0CZE/LpFV7gHeFfEDQFzYcSfWMF7vRZYfzrJQI8Y5CbppsFyQX3Yo
wSo+fLfp8I73rgNkeJnVPhTXy1MaEW3Gp2+Gd/QrncKwAt4Q2F08XPqXBQemD0jW
vFuvul/7t5DeCMgiUKRmgY24A1ekoA5F+iAfNHKBuREL8JtHX4Bm960XMQ4KoRc4
ziivnj9wYwyGmBvYilwTUK9r1WyGE+KIEPvp61iiW+8tNjksq3ITl/VVykEhMywR
YtxeAY+t2IBiFfhPScvNuDQlK6XKNP1yWN3Qf6tzLN9Dq01tu8I=
=VJMX
-----END PGP PUBLIC KEY BLOCK-----
